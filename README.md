## RMX2020-userdebug 12 SP2A.220305.012 eng.lordsa.20220308.024127 test-keys
- Manufacturer: realme
- Platform: mt6768
- Codename: RMX2020
- Brand: realme
- Flavor: RMX2020-userdebug
- Release Version: 12
- Id: SP2A.220305.012
- Incremental: eng.lordsa.20220308.024127
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: realme/RMX2020/RMX2020:12/SP2A.220305.012/neolit03072355:userdebug/test-keys
- OTA version: 
- Branch: RMX2020-userdebug-12-SP2A.220305.012-eng.lordsa.20220308.024127-test-keys
- Repo: realme_rmx2020_dump_27151


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
